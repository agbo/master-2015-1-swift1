// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Jedi.swift instead.

import CoreData

enum JediAttributes: String {
    case midichlorians = "midichlorians"
    case name = "name"
}

enum JediRelationships: String {
    case master = "master"
    case padawan = "padawan"
}

@objc
class _Jedi: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "Jedi"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _Jedi.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var midichlorians: NSNumber?

    // func validateMidichlorians(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var name: String?

    // func validateName(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

    @NSManaged
    var master: Jedi?

    // func validateMaster(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var padawan: Jedi?

    // func validatePadawan(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

}

