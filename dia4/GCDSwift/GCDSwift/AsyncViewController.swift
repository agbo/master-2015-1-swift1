//
//  AsyncViewController.swift
//  GCDSwift
//
//  Created by Fernando Rodríguez Romero on 07/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

import UIKit

class AsyncViewController: UIViewController {

    @IBOutlet weak var photoView: UIImageView!
    
    // MARK: - Actions
    
    @IBAction func asyncDownload(sender: AnyObject) {
        
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), { () -> Void in
            
            if let theURL = NSURL(string: "http://bit.ly/gemelasJaramillo"),
                let data = NSData(contentsOfURL: theURL),
                let image = UIImage(data: data){
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.photoView.image = image
                    })
                    
            }

        })
    }
    
    
    @IBAction func syncDownload(sender: AnyObject) {
        
        if let theURL = NSURL(string: "http://bit.ly/gemelasJaramillo"),
            let data = NSData(contentsOfURL: theURL),
            let image = UIImage(data: data){
                
                photoView.image = image
        }
        
        
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
