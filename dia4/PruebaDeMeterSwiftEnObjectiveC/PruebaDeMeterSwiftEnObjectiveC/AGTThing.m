//
//  AGTTing.m
//  PruebaDeMeterSwiftEnObjectiveC
//
//  Created by Fernando Rodríguez Romero on 07/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTThing.h"

@implementation AGTThing

-(id) initWithName:(NSString*) name{
    if (self = [super init]) {
        _name = name;
    }
    return self;
}
@end
