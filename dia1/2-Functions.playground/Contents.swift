// Funciones en SWift


// Functions 101

// Only internal name
func h(a: Int, b: Int) -> Int{
    return a * b;
}

h(3, 4);


// same internal & external name
func hh(#a : Int, #b: Int) ->Int{
    
    return a + b;
}

hh(a: 34,b: 67);

// Simular sintaxis de ObjC
func add(a: Int, b:Int, thenMultiplyBy c: Int) -> Int{
    return (a + b) * c;
}

add(3, 5, thenMultiplyBy: 45)

// Valores por defecto
func addSuffix(a:String, suffix : String = "ingly") -> String{
    return a + suffix;
}
addSuffix("accord")
addSuffix("Objective ", suffix: "C")


// Modificar parámtros
func f(var a:Int , b: Int) -> Int{
    a = a + 2
    return a + b;
}

// Return values
func namesOfNumbers(a:Int) -> (Int, String, String){
    var val : (Int, String, String)
    
    switch a{
    case 1:
        val = (1, "one", "uno")
    case 2:
        val = (2, "two", "dos")
        
    default:
        val = (a, "Go check google translator", "Búscate la vida")
        
    }
    return val
}


// Asignación con "pattern matching"
var (_, en, es) = namesOfNumbers(1)
en
es


////////////////////////////////////////
//  Funciones De Alto Nivel
//

// Funciones como parámetros
typealias IntToIntFunc = (Int) ->Int


func apply(f:IntToIntFunc, n:Int) -> Int{
    return f(n)
}

func double(a:Int) ->Int{
    return a * 2
}

func add42(a:Int) ->Int{
    return a + 42
}

apply(double, 45)
apply(add42, 42)

// Funciones que devuelven otras funciones
func compose(f:IntToIntFunc, g:IntToIntFunc) -> IntToIntFunc{
    
    func comp(a:Int) ->Int{
        return f(g(a))
    }
    return comp
}

var comp = compose(add42, double)

// Las pueda meter en colecciones
var funcs = [double, add42, comp, compose(comp, add42)]

for f in funcs{
    f(12)
}





















