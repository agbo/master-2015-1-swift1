// Primeros pasos con Swift


// Vars & constantes
var size : Float = 42.42
var name = "Anakin"


// TODO es un objeto
Int.max
Double.abs(-34.999)

// Conversiones: siempre via un inicializador
let answer = Double(42)
//let s = Int(" ")

// Typealias: typedef de C
// Muy util cuando tratanos con funciones
typealias Integer = Int
var x : Integer = 34

// Colecciones básicas
var swift = "Brand new language from Apple"
swift = swift + "aaaaa"


// Arrays
var words = ["uno", "dos", "three"];
//var wordz = ["uno", "dos", "three", 45]; // no sabe convertir 45 en string
// Dictionary
var numberNames = [1: "one", 2: "two", 56 : "lucas"];

// Iterate
var tally = ""
var nums = [1,2,3,4,5,6]
for element in nums{
    tally = "\(tally) \(element)"
}

for (key, value) in numberNames{
    println("\t\(key)\t\(value)")
}

// Tuplas
var pair = (1, "one")

// Access
pair.0
pair.1
pair.1 = "uno"


// One last thing!
for i in Range(start: 1, end: 5){
    println(i)
}

for i in 1..<5{
    println(i)
}














