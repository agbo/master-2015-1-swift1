// Optional
// Una caja donde puedes meter cosas, incuido un nil

// EMPAQUETADO
var a : Int? = 42
var b : Int?
var c = Optional("Hola")
var d : String? = " Mundo"

// Estilo Inception
var e: String?? = c


// DESEMPAQUETADO

// De forma sensata
if let hola = c,
    world = d{
        
    // Si había algo en c, lo tengo en hola
    println(hola + world)
}

// A lo bestia
println(c!) // Pide a Jobs que haya algo dentro

// Optonional Chaining
var z = 3
z.successor()

// Forma manual
if let insideValue = a{
    insideValue.successor()
}
// Más rápido con chaining
a?.successor()














