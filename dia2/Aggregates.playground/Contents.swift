// Agregados: Enums, Structs & Clases

// EXTENSIONS

extension String{
   // Reverse
    
    func reverse() -> String{
        var r = ""
        r.extend(Swift.reverse(self))
        return r
    }
}

var h = "Hola"
h.reverse()


// ENUM
enum LightSaberColor{
    case Blue, Red, Green, Purple
}


// STRUCT
struct LightSaber{
    
    // Propiedad estática 
    static let  quote = "An elegant weapon for a more civilized age"
    
    // Propiedades de instancia
    var color : LightSaberColor = .Blue {
        // Observador de propiedad
        willSet{
            println("About to change color to \(color)")
        }
    }
    
    var isDoubleBladed = false
}

var saber = LightSaber()
LightSaber.quote
saber.isDoubleBladed
saber.color
saber.color = .Red


// CLASSES
class Sentient: Printable {
    
    var name = ""
    
    
    // Inits
    init(){}                // default init
    
    init(name: String){     // designate
        self.name = name
    }
    
    
    // protocolos
    // Printable
    var description : String{
        return "<Sentient being named \(name)"
    }
}

var unknown = Sentient()
var jabba = Sentient(name: "Jabba the Hutt")
println(jabba)

//var karnak : Sentient = "Karnak"



class Jedi: Sentient{
    
    // Propiedades de instancia
    var midichlorians = 4200
    var lightSaber : LightSaber = LightSaber(color: .Blue,
        isDoubleBladed: false)
    var master : Jedi?
    
    // Inits
    // Designado: crea un Jedi completo, alicatado hasta el techo
    // con maestro incluido.
    convenience init(name: String,
        saber: LightSaber, midichlorians: Int, master: Jedi?){
        
            self.init(name: name)
            
            self.lightSaber = saber
            self.midichlorians = midichlorians
            self.master = master
    }
    
    // Crea un maestro Jedi: 10K midichlorianos por defecto
    // las demás propiedades, quedan por defecto
    convenience init(masterName: String){
        self.init(name: masterName)
        midichlorians = 10_000
        
    }
    
    
    // Métodos
    func totalMidichlorians() -> Int{
        // Los suyos + los de su maestro
        var total = 0
        
        if let theMaster = master{
            // Si el master no era nil, aqui le tengo
            total = theMaster.midichlorians
        }
    
        // los del maestro y los mios
        total = total + midichlorians
        
        return total
    }
    
    
    func pomo() ->String{
        return "█"
    }
    
    func empuñadura() ->String{
        return "||||||(•)"
    }
    
    func hoja()->String{
        return "█Ξ████████████████████"
    }
    
    func unsheathe() -> String{
        
        // ¡Funciones dentro de funciones!
        func saber()->String{
            return pomo() + empuñadura() + hoja()
        }
        
        func doubleSaber()->String{
            return hoja().reverse() + empuñadura() + hoja()
        }
        
        if lightSaber.isDoubleBladed{
            return doubleSaber()
        }else{
            return saber()
        }
    }
}

// Creamos unos jedis
var obiWan = Jedi(masterName: "Obi Wan Kenobi")
var anakin = Jedi(name: "Anakin Skywalker", saber: LightSaber(), midichlorians: 20_000, master: obiWan)

var k = Jedi(masterName: "Yo qué sé")
k.lightSaber = LightSaber(color: .Purple, isDoubleBladed: true)

k.unsheathe()

obiWan.totalMidichlorians()
anakin.totalMidichlorians()

obiWan.unsheathe()

class Sith : Jedi {
    
    // Init
    
    // Crea un Señor Oscuro del Sith
    convenience init(darkLordName: String){
        
        self.init(masterName: darkLordName)
        lightSaber = LightSaber(color: .Red, isDoubleBladed: false);
        
    }
    
    // Cambiamos la implementación de los
    // componentes de la espada.
    // Pero el método más complejo ya es
    // 100% genérico
    override func empuñadura() -> String {
        return "◚◚◚◚◚ịị▔ịị"
    }
    
    override func hoja() -> String {
        return "≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣"
    }
    
    override func pomo() -> String {
        return "ị▔ịị"
    }
}

var darthVader = Sith(darkLordName: "Darth Vader");
darthVader.unsheathe()

var darthMaul = Sith(darkLordName: "Darth Maul")
darthMaul.lightSaber = LightSaber(color: .Red, isDoubleBladed: true)

darthMaul.unsheathe()


// Casting & Info de tipo
if (obiWan is Jedi){
    println("Pues sí, es un Jedi")
}

// Upcasting: seguro
var youngAnakin = darthVader as Jedi

// Downcasting: prohibido
//var nooooo = obiWan as! Sith

// Downcasting, the safe way
if let never = (obiWan as? Sith){
    println("Obi wan would never give himself to the Dark Side!")
}

















