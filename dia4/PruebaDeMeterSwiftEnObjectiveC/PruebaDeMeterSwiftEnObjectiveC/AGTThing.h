//
//  AGTTing.h
//  PruebaDeMeterSwiftEnObjectiveC
//
//  Created by Fernando Rodríguez Romero on 07/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTThing : NSObject

@property (nonatomic, strong) NSString *name;

-(id) initWithName:(NSString*) name;

@end
