//
//  SwiftModels.swift
//  PruebaDeMeterSwiftEnObjectiveC
//
//  Created by Fernando Rodríguez Romero on 07/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

import Foundation


class WeirdThing: AGTThing {
    
    
    var number = 42
    
    convenience init(number: Int, name: String){
        self.init(name: name)
        self.number = number
    }
    
}

