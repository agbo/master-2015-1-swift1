// Genéricos

// Función específica
func flip(inout a: String, inout b: String){
    
    var aux = a;
    a = b
    b = aux
    
}

var (name, surname) = ("Yoda", "Minch")
flip(&name, &surname)
(name, surname)


// Función genérica
func flip<T>(inout a: T, inout b: T){
    
    var aux = a;
    a = b
    b = aux
}

var (e, pi) = (3.1415926, 2.71828)
flip(&e, &pi)
(e,pi)

var (a,b) = ([1,2,3], [3,2,1])
flip(&a, &b)
(a,b)


// Agregados genéricos

var numbers : Array<Double> = [2.9, 3.15, 5.4]


// Enums genéricas
var f : Optional<String> = Optional("Hola")
var ff :String? = "Hola"
var fff : Optional<Optional<String>> = Optional(f)


// Una pila genérica
struct Stack<T>{
    
    // Vars
    var _stack = Array<T>()
    
    // Deberes
    // crear una inicializadror que recibe un Array<T>
    
    var count : Int{
        get{
            // devuelve el total de objetos que hay en la pila
            return _stack.count
        }
    }
    
    // Funciones
    mutating func push(item: T){
        // añade item a la cabecera de la pila
        _stack.insert(item, atIndex: 0)
    }
    
    mutating func pop() -> T{
        // quita lo que está en la cabeza de la pila
        // y lo devuelve
        return _stack.removeAtIndex(0)
    }
    
    func peek() -> T{
        // devuelve lo que está en la cabeza, pero sin
        // cambiar nada
        return _stack[0]
    }
    
}

var nums = Stack<Int>()

nums.push(34)
nums.push(0)
nums.count
nums.peek()
nums.pop()
nums.count



struct Pair <T : Equatable, U: Equatable> {
    let head : T
    let tail : U
}

var p = Pair(head: 33, tail: "Hola, Hola")

p.head
p.tail

// NO funciona porque Pair<T,U> es constante
//p.head = 56

// No funciona, porque Stack<T> no implementa Equatable.
// ¿Por qué no intentas implementarlo?
//var q = Pair(nums, "Esto no deberia de colar")



















