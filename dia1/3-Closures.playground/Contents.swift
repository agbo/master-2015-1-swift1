// Closures

"All functions are actually closures: blocks with a less"
"crazy syntax"


// funcs vs closures
func g(a:Int) -> (Int){
    return a + 1;
}

let gg = {(a:Int) -> Int
    in
    return a + 1
}

g(3)
gg(3)

// Capturan el entorno léxico
let ggg = {(a:Int) -> Int in return gg(g(4))}

ggg(1)


// Distintas formas de sintaxis de clausura
var closures = [ g,
    {(a:Int) -> Int in return a - 42},
    {a in return a + 45},
    {a in a / 42},
    {$0 * 42}
]

for closure in closures{
    println(closure(3))
}

// Los operadores también son clausuras
typealias BinaryFunc = (Int, Int) ->Int

let  applier = { (f:BinaryFunc, m:Int, n:Int) -> Int
    in
    return f(m,n)
}

applier(*, 2, 4)



// Sintaxis de la "trailing closure": al que le cuelga la clausura

func applierInv(m:Int, n:Int, f:BinaryFunc) -> Int{
    return applier(f, m,n)
}

var z = applierInv(3, 2, {$0 + $1})

// equivalent a
applierInv(3,2){
    return $0 + $1
}













